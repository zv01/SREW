import csv


def write_metadata(timestamp, person_id, sex, age, dominant_hand, is_wearing_glasses, is_after_physical_exc):
    with open('data.csv', 'a+', newline='') as file:
        writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow([timestamp, person_id, sex, age, dominant_hand, is_wearing_glasses, is_after_physical_exc])
