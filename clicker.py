from win32con import *
from win32api import *
from win32ui import *
from win32gui import *
from win32event import *
import win32process
from time import time, ctime, sleep
import sys

# Captions (titles) of popup windows to confirm
# EDIT THIS
PopupNames = (
    'Warning',
    'Error',
    'Information',
    'Install Dialler',  # You better delete this line :-)
)


def GetWindowText(Window):
    """
    Get text of all 'Static' elements of windows and return
  concatenated.
    """
    Child, Text = None, ''
    while 1:
        try:
            Child = FindWindowEx(Window, Child, 'Static', None)
        except:
            break
        Text += '\n\t'.join(Child.GetWindowText().split('\r'))
    return Text


def FindControl(Window, CName='OK', CType='Button'):
    """
    Find control with name CName in Window

    @arg Window: Top level window
    @type Window: PyCWnd
    @arg CName: Control Name
    @type CName: string
    @arg CType: Control class
    @type CType: string
    @return Control
    @rtype: PyCwnd
    """
    return FindWindowEx(Window, None, CType, CName)


def ConfirmDialog(Window, BName=None, Delay=0.5):
    """
    Find button with name BName in Window and simulate a button
  activation.

    @arg WName: Window Name
    @type WName: string
    @arg BName: Button Name
    @type BName: string
    @return: Button in case of success, negative error code else
    @rtype: PyCWnd
    """
    # Find Button
    Button = FindControl(Window, BName)
    Button.SendMessage(BM_SETSTATE, 1, 0)
    sleep(Delay)  # Window should show up at least for half a second.

    # Simulate button press to confirm window
    idButton = Button.GetDlgCtrlID()
    hButton = Button.GetSafeHwnd()
    Caption = Window.GetWindowText()
    Window.SendMessage(WM_COMMAND, MAKELONG(idButton, BN_CLICKED),
                       hButton)

    print(ctime(time()))
    print("Confirmed '%s'" % Caption)
    return Button


if __name__ == '__main__':
    # Program parameters and options
    ConfDelay = 0.5
    if '-i' in sys.argv:
        ConfDelay = 0

    print(ctime(time()))
    print("%s started" % sys.argv[0])

    # Scripts\pywin32_postinstall.py -install

    Window = FindWindow(None, "HandAQUS")
    print(Window)

    def _windowEnumerationHandler(win, extra):
        print('Child win')
        print(win)
        print(extra)

    windows = []
    EnumChildWindows(Window, _windowEnumerationHandler, windows)
    print(windows)




exit(0)
    # while 1:
    #     print('trying...')
    #     for Name in PopupNames:
    #         try:
    #             Window = FindWindow(None, Name)
    #         except:
    #             continue
    #
    #         print('window found..')
    #         # Extract text information from Popup (for logging).
    #         Message = GetWindowText(Window)  # Get message text before win disappears
    #         ConfirmDialog(Window, 'OK', ConfDelay)
    #
    #         if Message:
    #             print('\t' + Message)
    #             break
    #
    #     sleep(1)

