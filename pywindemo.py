import time
import pywinauto
from pywinauto import *
from pywinauto.application import Application

app = pywinauto.Application().start('C:\\APP\\HandAQUS\\HandAQUS.exe', timeout=10)
main_dlg = app.window(title='HandAQUS')
main_dlg.wait('visible')

time.sleep(5)

main_dlg.set_focus()
main_dlg.type_keys("%s")  # Clicks Settings

save_dlg = app.window(title='Save As')
file_path = 'C:\Temp\handwriting.dat'
file_name_field = save_dlg['5']
file_name_field.type_keys(file_path)
save_dlg.Save.click()
#main_dlg.close()


# interact with child control

# Print all controls on the dialog
#win.print_control_identifiers()

