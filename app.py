from PyQt5 import QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from exercise import Exercise
from broker import MQServer
from layout.main import Ui_MainWindow
from functools import partial
import math
import sys
import logging
import metadata
import time
import yaml

import random
import hashlib

log = logging.getLogger('bioaq')
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

config = None

real_id = None
per_id = None

exc = None  # type: Exercise


def show_warning_message(text):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Warning)
    msg.setText(text)
    msg.setWindowTitle("Warning")
    msg.exec_()


def show_error_message(text):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(text)
    msg.setWindowTitle("Error")
    msg.exec_()


def show_confirmation_message(text):
    qm = QMessageBox()
    qm.setWindowTitle("Confirmation")
    ret = qm.question(app.activeWindow(), '', text, qm.Yes | qm.No)

    if ret == qm.Yes:
        return True
    return False


def count_person_id():
    global per_id, real_id
    print('Real ID {}'.format(real_id))
    per_id = int(hashlib.sha256(str(real_id).encode('utf-8')).hexdigest(), 16) % 10 ** 6


def create_exercise(shortcut):
    global exc, per_id
    exc = Exercise(shortcut, per_id)
    exc.create_dir()
    exc.start_exercise()
    return True


def stop_exercise():
    global exc
    exc.stop_exercise()
    exc = None


def start_exercise_recording() -> bool:
    global exc
    print('starting recording...')
    if exc:
        exc.start_recording_related()
        return True
    else:
        return False


class MQThread(QThread):
    icon_signal = pyqtSignal(str, bool, name='iconSignal')
    recording_signal = pyqtSignal(name='recordingSignal')

    def __init__(self):
        QThread.__init__(self)

    def __del__(self):
        self.wait()

    def run(self):
        server = MQServer()
        print('[TH-MQ] thread started')
        while True:
            #  Wait for next request from client
            message = server.socket.recv()
            print("Received request: %s" % message)
            if message == b'HandAQUS ready':
                print('Received HandAQUS ready message.')
                self.icon_signal.emit('hq', True)
                server.socket.send(b"ACK")

            if message == b'HandAQUS closed':
                print('Received HandAQUS closing message.')
                self.icon_signal.emit('hq', False)
                server.socket.send(b"ACK")

            if message == b'HandAQUS pen touched':
                print('Received HandAQUS first touch message.')
                self.icon_signal.emit('touch', True)
                self.recording_signal.emit()
                server.socket.send(b"ACK")


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        global config
        super(MainWindow, self).__init__()

        with open('config.yaml') as f:
            config = yaml.safe_load(f)
        if config is None or 'exercises' not in config:
            show_error_message('Unable to fetch the configuration from config.yaml file.')
            exit(2)

        self.move(10, 3)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        ui = self.ui
        ui.le_idnumber.setValidator(QIntValidator())
        ui.le_age.setValidator(QIntValidator())
        ui.pb_saveMeta.clicked.connect(self.on_metadata_save_button)
        ui.pb_discardExc.clicked.connect(self.discard_exercise_button)

        # create exercise buttons
        self.exc_buttons = []

        i = 1
        for shortcut, name in config['exercises'].items():
            button = QPushButton("%02d - %s" % (i, name))
            button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            button.clicked.connect(partial(self.on_exercise_button, shortcut=shortcut, button=button))
            button.setEnabled(False)
            self.ui.grid_excButtons.addWidget(button, math.ceil(i / 2), 2 if i % 2 == 0 else 1)
            self.exc_buttons.append(button)
            i += 1

        # create app assets pixmaps
        self.red_pixmap = QPixmap('assets/red.png')
        self.green_pixmap = QPixmap('assets/green.png')
        self.discard_icon = QIcon('assets/cross.png')
        self.prepare_exercise_ui()

        # running MQ thread
        self.mq_thread = MQThread()
        self.mq_thread.icon_signal.connect(self.update_status_icon)
        self.mq_thread.recording_signal.connect(start_exercise_recording)
        self.mq_thread.start()


    def discard_exercise_button(self):
        global exc
        exc.discard_exercise()
        exc = None
        self.enable_exercise_status_indicators(False)
        self.enable_exercises_buttons(True)
        self.ui.pb_discardExc.setEnabled(False)

    def update_status_icon(self, icon: str, status: bool = True):
        print('updating status icon...')
        icon_dict = {
            'eeg': self.ui.lb_statusEEG,
            'touch': self.ui.lb_statusTouch,
            'hq': self.ui.lb_statusHQ
        }

        if icon in icon_dict:
            icon_dict[icon].setPixmap(self.green_pixmap if status is True else self.red_pixmap)

    def on_metadata_save_button(self):
        global per_id, real_id
        ui = self.ui
        log.info('saving metadata...')

        # generate person id
        if ui.le_idnumber.text() is not '':
            real_id = int(ui.le_idnumber.text())
        else:
            real_id = random.randint(1, 9999)

        count_person_id()
        ui.lb_assignedId.setText(str(per_id))

        age = ui.le_age.text()
        if not age:
            show_warning_message('Please fill the subject age.')
            return

        if ui.rb_male.isChecked():
            sex = 'male'
        elif ui.rb_female.isChecked():
            sex = 'female'
        else:
            show_warning_message('Please select the gender.')
            return

        if ui.rb_leftHanded.isChecked():
            hand = 'left'
        elif ui.rb_rightHanded.isChecked():
            hand = 'right'
        else:
            show_warning_message('Please select the person\'s dominate hand.')
            return

        glasses = ui.cb_glassesWearing.isChecked()
        after_physical = ui.cb_afterPhysical.isChecked()

        metadata.write_metadata(time.strftime("%d-%m-%Y %H:%M:%S"), per_id, sex, age, hand, int(glasses),
                                int(after_physical))
        self.enable_exercises_buttons()

    def on_exercise_button(self, shortcut, button):
        global exc
        print('exercise button pressed: %s' % shortcut)

        if exc:
            button.setStyleSheet("background-color: orange")
            app.processEvents()
            stop_exercise()
            self.enable_exercises_buttons(True)
            self.enable_exercise_status_indicators(False)
            self.ui.pb_discardExc.setEnabled(False)

        else:
            self.enable_exercises_buttons(False)
            self.prepare_exercise_ui()

            print('creating exercise object')

            if create_exercise(shortcut):
                button.setStyleSheet("background-color: lightgreen")
                button.setEnabled(True)
                self.ui.pb_discardExc.setEnabled(True)
                self.enable_exercise_status_indicators(True)
            else:
                exc = None
                self.enable_exercises_buttons(True)

    def enable_exercises_buttons(self, enable=True):

        for button in self.exc_buttons:
            button.setEnabled(enable)
            button.setStyleSheet("background-color: none")

    def enable_exercise_status_indicators(self, enable=True):
        self.ui.lb_statusHQ.setEnabled(enable)
        self.ui.lb_statusEEG.setEnabled(enable)
        self.ui.lb_statusTouch.setEnabled(enable)

    def prepare_exercise_ui(self):
        self.ui.lb_statusHQ.setPixmap(self.red_pixmap)
        self.ui.lb_statusEEG.setPixmap(self.red_pixmap)
        self.ui.lb_statusTouch.setPixmap(self.red_pixmap)
        self.ui.pb_discardExc.setIcon(self.discard_icon)


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    win = MainWindow()
    try:
        win.show()
    except Exception as e:
        print(e)

    sys.exit(app.exec())
