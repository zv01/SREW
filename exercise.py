import os
import shutil
import errno
import logging

import pywinauto
import subprocess
import signal

log = logging.getLogger('bioaq')


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


class Exercise:

    def __init__(self, shortcut: str, person_id: int, round: int = 1):
        self.round = round
        self.shortcut = shortcut
        self.person_id = person_id
        self.dir_name = "data/p%06d/" % person_id
        print('Creating Exercise: ' + str(self))

        # application references
        self.haqus_app = None
        self.haqus_main = None
        self.eeg_proc = None

    def __str__(self):
        return "[EXERCISE-{}] Person {}".format(self.shortcut, self.person_id)

    def get_exercise_name(self, extension):
        return 'p%06d_%s_%02d.%s' % (self.person_id, self.shortcut, self.round, extension)

    def create_dir(self):
        mkdir_p(self.dir_name)

    def start_exercise(self):
        log.info('starting: ' + str(self))
        #self.start_handaqus()
        self.start_stu()

    def start_recording_related(self):
        log.info('starting recording related: ' + str(self))
        self.start_eeg_recording()

    def discard_exercise(self):
        log.info('discarding: ' + str(self))
        self.stop_handaqus()
        self.stop_eeg_recording()
        rec_path = self.dir_name + self.get_exercise_name('svc')
        rec_path_eeg = self.dir_name + self.get_exercise_name('eeg')
        if os.path.isfile(rec_path):
            os.remove(rec_path)

        if os.path.isfile(rec_path_eeg):
            os.remove(rec_path_eeg)

    def stop_exercise(self):
        log.info('stopping: ' + str(self))
        self.stop_handaqus()
        self.stop_eeg_recording()

    def start_handaqus(self):
        rec_path = os.path.abspath(self.dir_name + self.get_exercise_name('svc'))
        self.haqus_app = pywinauto.Application().start(
            'C:\\APP\\HandAQUS\\HandAQUS.exe --mqport=5555 {}'.format(rec_path),
            timeout=10)
        self.haqus_main = self.haqus_app.window(title='HandAQUS')
        self.haqus_main.move_window(445, 0, 1450, 1080)
        self.haqus_main.wait('visible')

    def emulate_saving(self):
        self.haqus_main.set_focus()
        self.haqus_main.type_keys("%s")  # Clicks Settings
        save_dlg = self.haqus_app.window(title='Save As')
        file_path = 'C:\\Temp\\handwriting.svc'
        file_name_field = save_dlg['5']
        file_name_field.type_keys(file_path)
        save_dlg.Save.click()
        self.haqus_main.close()

    def stop_handaqus(self):
        self.haqus_main.close()

    def start_stu(self):
        rec_path = os.path.abspath(self.dir_name + self.get_exercise_name('stu'))
        self.haqus_app = pywinauto.Application().start(
            'C:\\APP\\STUCapture\\STUCaptureApp.exe --mqport=5555 {} {}'.format(rec_path,
                                                                                "Exc:" + self.shortcut),
            timeout=10)
        self.haqus_main = self.haqus_app.window(title='STUCapture')
        #self.haqus_main.move_window(445, 0, 1450, 1080)
        self.haqus_main.wait('visible')

    def start_eeg_recording(self):
        rec_path = os.path.abspath(self.dir_name + self.get_exercise_name('eeg'))
        self.eeg_proc = subprocess.Popen("C:\\APP\\LabRecorder\\LabRecorderCLI.exe {} 'type=EEG'".format(rec_path))

    def stop_eeg_recording(self):
        self.eeg_proc.send_signal(signal.SIGTERM)
        self.eeg_proc.wait()

    def get_existing_records(self):
        exists = []
        if os.path.isfile(self.dir_name + self.get_exercise_name('eeg')):
            exists.append('EEG')

        if os.path.isfile(self.dir_name + self.get_exercise_name('svc')):
            exists.append('Handwriting')

        return exists
