from exercise import Exercise
import time

e = Exercise(1, 1111111)
e.create_dir()
e.start_eeg_recording()
time.sleep(5)
e.stop_eeg_recording()
